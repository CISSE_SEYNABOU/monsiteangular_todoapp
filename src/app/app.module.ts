import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuAppSenegalComponent } from './menu-app-senegal/menu-app-senegal.component';
import { SenegalSliderComponent } from './senegal-slider/senegal-slider.component';
import { SenegalHistoireComponent } from './senegal-histoire/senegal-histoire.component';
import { SenegalGeographieComponent } from './senegal-geographie/senegal-geographie.component';
import { SenegalPolitiqueComponent } from './senegal-politique/senegal-politique.component';
import { SenegalEconomieComponent } from './senegal-economie/senegal-economie.component';
import { SenegalCultureComponent } from './senegal-culture/senegal-culture.component';
import { GreenSectionTitleDirective } from './green-section-title.directive';
import { AutoCodePipe } from './auto-code.pipe';
import { TodoListComponent } from './todo-list/todo-list.component';
import { ExportRegionsService} from './services/export-regions.service';

@NgModule({
  imports:[
    BrowserModule,
    BrowserAnimationsModule
  ],
  declarations: [
    AppComponent,
    MenuAppSenegalComponent,
    SenegalSliderComponent,
    SenegalHistoireComponent,
    SenegalGeographieComponent,
    SenegalPolitiqueComponent,
    SenegalEconomieComponent,
    SenegalCultureComponent,
    GreenSectionTitleDirective,
    AutoCodePipe,
    TodoListComponent,
   


  ],
 
  
  providers: [ ExportRegionsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
