import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenegalSliderComponent } from './senegal-slider.component';

describe('SenegalSliderComponent', () => {
  let component: SenegalSliderComponent;
  let fixture: ComponentFixture<SenegalSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenegalSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenegalSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
