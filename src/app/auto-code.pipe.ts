import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  
  name: 'autoCode'
  
})
export class AutoCodePipe implements PipeTransform {

  transform(nom:string): string {
    return nom+'(SN)';
  }

}
