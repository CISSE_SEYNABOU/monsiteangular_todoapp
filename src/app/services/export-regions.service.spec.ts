import { TestBed } from '@angular/core/testing';

import { ExportRegionsService } from './export-regions.service';

describe('ExportRegionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExportRegionsService = TestBed.get(ExportRegionsService);
    expect(service).toBeTruthy();
  });
});
