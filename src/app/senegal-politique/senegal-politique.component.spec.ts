import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenegalPolitiqueComponent } from './senegal-politique.component';

describe('SenegalPolitiqueComponent', () => {
  let component: SenegalPolitiqueComponent;
  let fixture: ComponentFixture<SenegalPolitiqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenegalPolitiqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenegalPolitiqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
